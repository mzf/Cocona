#include "SQLiteDataBase.h"

#include <sqlite3.h>
#include <sstream>
#include <stdexcept>

SQLiteDataBase::SQLiteDataBase(const std::string &filePath) {
  auto rc = sqlite3_open(filePath.c_str(), &this->db);
  if (rc != SQLITE_OK) {
    std::stringstream message;
    message << "Can't open in-memory SQLite database: "
            << sqlite3_errmsg(this->db);
    sqlite3_close(this->db);
    throw std::runtime_error(message.str());
  }
}

SQLiteDataBase::~SQLiteDataBase() { sqlite3_close(this->db); }

void SQLiteDataBase::RunQuery(const std::string &query) {
  auto rc = sqlite3_exec(db, query.c_str(), nullptr, nullptr, nullptr);
  if (rc != SQLITE_OK) {
    std::stringstream message;
    message << "SQLite Error while exec: \n"
            << query << "\n SQLite error message: " << sqlite3_errmsg(this->db);
    throw std::runtime_error(message.str());
  }
}

SQLiteDataBase::SelectResult
SQLiteDataBase::RunSelect(const std::string &query) {

  SelectResult result;

  sqlite3_stmt *stmt = nullptr;
  auto rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);
  if (rc != SQLITE_OK) {
    std::stringstream message;
    message << "SQLite Error while RunSelect/sqlite3_prepare_v2: \n"
            << query << "\n SQLite error message: " << sqlite3_errmsg(this->db);
    throw std::runtime_error(message.str());
  }
  while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
    SelectResultPair resultPair;
    auto columnCount = sqlite3_column_count(stmt);
    if (columnCount > 0) {
      auto type = sqlite3_column_type(stmt, 0);
      if (type == SQLITE_INTEGER) {
        resultPair.first = sqlite3_column_int(stmt, 0);
      } else {
        resultPair.first =
            reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
      }
    }
    if (columnCount > 1) {
      auto type = sqlite3_column_type(stmt, 1);
      if (type == SQLITE_INTEGER) {
        resultPair.second = sqlite3_column_int(stmt, 1);
      } else {
        resultPair.second =
            reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
      }
    }
    result.push_back(resultPair);
  }
  if (rc != SQLITE_DONE) {
    std::stringstream message;
    message << "SQLite Error while RunSelect/sqlite3_step: \n"
            << query << "\n SQLite error message: " << sqlite3_errmsg(this->db);
    throw std::runtime_error(message.str());
  }
  rc = sqlite3_finalize(stmt);
  if (rc != SQLITE_OK) {
    std::stringstream message;
    message << "SQLite Error while RunSelect/sqlite3_finalize: \n"
            << query << "\n SQLite error message: " << sqlite3_errmsg(this->db);
    throw std::runtime_error(message.str());
  }

  return result;
}
