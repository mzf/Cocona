#ifndef GIT_H
#define GIT_H

#include <memory>
#include <string>
class GitRepository;

class Git {
public:
  Git();
  ~Git();

  std::shared_ptr<GitRepository> GetGitRepository(const std::string &url,
                                                  const std::string &localPath);

  static void CheckErrorCode(int gitErrorCode);
};

#endif // GIT_H
