#ifndef SQLITE_DATA_BASE_H
#define SQLITE_DATA_BASE_H

#include <string>
#include <variant>
#include <vector>

class sqlite3;

class SQLiteDataBase {
public:
  SQLiteDataBase(const std::string &filePath);
  ~SQLiteDataBase();

  void RunQuery(const std::string &query);
  typedef std::pair<std::variant<int, std::string>,
                    std::variant<int, std::string>>
      SelectResultPair;
  typedef std::vector<SelectResultPair> SelectResult;
  SelectResult RunSelect(const std::string &query);

private:
  sqlite3 *db;
};

#endif
