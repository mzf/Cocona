#ifndef GITCOMMIT_H
#define GITCOMMIT_H

#include <string>
class git_oid;
class git_commit;
class git_repository;

class GitCommit {
public:
  GitCommit(git_repository &repo, const git_oid &objectId);
  ~GitCommit();

  std::string GetAuthorEMail() const;
  std::string GetId() const;
  time_t GetTime() const;

private:
  git_oid *ObjectId;
  git_commit *Commit;
};

#endif // GITCOMMIT_H
