#ifndef TEMPORARYDIRECTORY_H
#define TEMPORARYDIRECTORY_H

#include <filesystem>
#include <string>

class TemporaryDirectory {
public:
  TemporaryDirectory();
  ~TemporaryDirectory();

  std::string GetFullPath() const;

private:
  std::filesystem::path Path;
};

#endif // TEMPORARYDIRECTORY_H
