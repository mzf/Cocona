#include "Git.h"
#include "GitRepository.h"

#include <git2.h>
#include <sstream>

Git::Git() {
  int gitErrorCode = git_libgit2_init();
  CheckErrorCode(gitErrorCode);
}

Git::~Git() {
  auto gitErrorCode = git_libgit2_shutdown();
  CheckErrorCode(gitErrorCode);
}

std::shared_ptr<GitRepository>
Git::GetGitRepository(const std::string &url, const std::string &localPath) {
  return std::make_shared<GitRepository>(url, localPath);
}

void Git::CheckErrorCode(int errorCode) {
  if (errorCode < 0) {
    std::stringstream errorMessage;
    errorMessage << "Git API Error " << errorCode;
    const git_error *e = git_error_last();
    if (e != nullptr) {
      errorMessage << "/" << e->klass << ": " << e->message;
    }
    throw std::runtime_error(errorMessage.str());
  }
}
