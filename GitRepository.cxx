#include "GitRepository.h"
#include "Git.h"
#include "GitCommitsWalker.h"

#include <git2.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

GitRepository::GitRepository(const std::string &url,
                             const std::string &localPath)
    : Repo(nullptr) {
  git_clone_options cloneOptions = GIT_CLONE_OPTIONS_INIT;
  cloneOptions.bare = 1;
  auto gitErrorCode =
      git_clone(&this->Repo, url.c_str(), localPath.c_str(), &cloneOptions);
  Git::CheckErrorCode(gitErrorCode);
}

GitRepository::~GitRepository() { git_repository_free(this->Repo); }

std::shared_ptr<GitCommitsWalker> GitRepository::CreateCommitsWalker() {
  return std::make_shared<GitCommitsWalker>(*this->Repo);
}
