#include "GitCommitsWalker.h"
#include "Git.h"
#include "GitCommit.h"

#include <git2.h>

GitCommitsWalker::GitCommitsWalker(git_repository &repo)
    : Walker(nullptr), Repo(repo) {
  auto gitErrorCode = git_revwalk_new(&this->Walker, &repo);
  Git::CheckErrorCode(gitErrorCode);
  gitErrorCode = git_revwalk_sorting(this->Walker,
                                     GIT_SORT_TOPOLOGICAL | GIT_SORT_REVERSE);
  Git::CheckErrorCode(gitErrorCode);
  gitErrorCode = git_revwalk_push_head(this->Walker);
  Git::CheckErrorCode(gitErrorCode);
}

GitCommitsWalker::~GitCommitsWalker() { git_revwalk_free(this->Walker); }

std::shared_ptr<GitCommit> GitCommitsWalker::GetNextCommit() {
  git_oid objectId;
  if (git_revwalk_next(&objectId, this->Walker)) {
    return {};
  }
  return std::make_shared<GitCommit>(this->Repo, objectId);
}
