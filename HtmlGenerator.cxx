#include "HtmlGenerator.h"
#include "CoconaConfig.h"
#include "ContributionDataBase.h"
#include "UserConfig.h"

#include <ctemplate/template.h>

#include <ctime>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <time.h>

HtmlGenerator::HtmlGenerator(const ContributionDataBase &db) : db(db) {}

void HtmlGenerator::Generate(const std::string &outputHtmlFilePath) const {

  // DEBUG
  auto reposDay = this->db.QueryCommitNumberByRepoByDay();
  for (auto &repo : reposDay) {
    std::cout << "Repo: " << repo.repoName << std::endl;
    auto &commits = repo.commitNumberByTime;
    for (auto &commit : commits) {
      std::cout << std::get<0>(commit) << " " << std::get<1>(commit)
                << std::endl;
    }
  }

  auto reposMonth = this->db.QueryCommitNumberByRepoByMonth();
  for (auto &repo : reposMonth) {
    std::cout << "Repo: " << repo.repoName << std::endl;
    auto &commits = repo.commitNumberByTime;
    for (auto &commit : commits) {
      std::cout << std::get<0>(commit) << " " << std::get<1>(commit)
                << std::endl;
    }
  }

  auto reposYear = this->db.QueryCommitNumberByRepoByYear();
  for (auto &repo : reposYear) {
    std::cout << "Repo: " << repo.repoName << std::endl;
    auto &commits = repo.commitNumberByTime;
    for (auto &commit : commits) {
      std::cout << std::get<0>(commit) << " " << std::get<1>(commit)
                << std::endl;
    }
  }

  // END DEBUG

  ctemplate::TemplateDictionary dictionnary("cocona");

  dictionnary.SetValue("CHARTJS_CONTENT", GetChartJsContent());

  auto name = this->db.GetUserConfig().GetProfileName();
  if (name.empty()) {
    name = "My Profile";
  }
  dictionnary.SetValue("PROFILE_NAME", name);

  auto picture = this->db.GetUserConfig().GetProfilePicture();
  if (!picture.empty()) {
    std::stringstream pictureHTML;
    pictureHTML << "<img src=\"" << picture << "\" ";
    pictureHTML << "alt=\"" << name << " profile picture\">";
    dictionnary.SetValue("PROFILE_PICTURE", pictureHTML.str());
  }

  dictionnary.SetIntValue("PROJECT_NUMBER", reposYear.size());
  for (auto &repo : reposYear) {
    auto section = dictionnary.AddSectionDictionary("PROJECTS");
    section->SetValue("NAME", repo.repoName);
  }

  dictionnary.SetIntValue("NUMBER_OF_COMMITS",
                          this->db.QueryTotalCommitNumber());
  auto firstCommitEpoch = this->db.QueryFirstCommitEpoch();
  dictionnary.SetValue("FIRST_COMMIT_DATE",
                       ConvertEpochToString(firstCommitEpoch));
  auto lastCommitEpoch = this->db.QueryLastCommitEpoch();
  dictionnary.SetValue("LAST_COMMIT_DATE",
                       ConvertEpochToString(lastCommitEpoch));

  std::string output;
  ctemplate::ExpandTemplate(DEFAULT_HTML_TEMPLATE_PATH, ctemplate::DO_NOT_STRIP,
                            &dictionnary, &output);

  std::fstream outputStream(outputHtmlFilePath, std::ios_base::out);
  if (!outputStream.good()) {
    std::stringstream message;
    message << "Can not open output file: " << outputHtmlFilePath;
    throw std::runtime_error(message.str());
  }

  outputStream << output;
}

std::string HtmlGenerator::GetChartJsContent() const {

  auto chartjsminFilePath = std::string(CHARTJS_INCLUDE_PATH) + "/chart.min.js";
  std::ifstream chartJsStream(chartjsminFilePath);
  if (!chartJsStream.is_open()) {
    std::stringstream message;
    message << "Error while opening the file " << chartjsminFilePath;
    throw std::runtime_error(message.str());
  }
  std::stringstream content;
  content << chartJsStream.rdbuf();
  return content.str();
}

std::string HtmlGenerator::ConvertEpochToString(const time_t &epoch) const {
  std::stringstream stream;
  stream << std::put_time(std::localtime(&epoch), "%c %Z");
  return stream.str();
}
