#ifndef USER_CONFIG_H
#define USER_CONFIG_H

#include <string>
#include <vector>

class UserConfig {
public:
  UserConfig(const std::string &filePath);

  typedef std::string Repository;
  const std::vector<Repository> &GetRepositories() const;

  typedef std::string Identity;
  const std::vector<Identity> &GetIdentities() const;

  const std::string &GetProfilePicture() const;
  const std::string &GetProfileName() const;

private:
  std::vector<Repository> repositories;
  std::vector<Identity> identities;
  std::string profilePicture;
  std::string profileName;
};

#endif // USER_CONFIG_H
