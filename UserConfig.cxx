#include "UserConfig.h"

#include <stdexcept>
#include <yaml-cpp/yaml.h>

UserConfig::UserConfig(const std::string &filePath) {

  auto config = YAML::LoadFile(filePath);
  auto repositoryNodes = config["repositories"];
  for (auto repositoryNode : repositoryNodes) {
    this->repositories.push_back(repositoryNode.as<std::string>());
  }

  auto identityNodes = config["identities"];
  for (auto identityNode : identityNodes) {
    this->identities.push_back(identityNode.as<std::string>());
  }

  auto profile = config["profile"];
  auto picture = profile["picture"];
  if (picture.IsDefined()) {
    this->profilePicture = picture.as<std::string>();
  }
  auto name = profile["name"];
  if (name.IsDefined()) {
    this->profileName = name.as<std::string>();
  }
}

const std::vector<UserConfig::Repository> &UserConfig::GetRepositories() const {
  return this->repositories;
}

const std::vector<UserConfig::Identity> &UserConfig::GetIdentities() const {
  return this->identities;
}

const std::string &UserConfig::GetProfilePicture() const {
  return this->profilePicture;
}

const std::string &UserConfig::GetProfileName() const {
  return this->profileName;
}
