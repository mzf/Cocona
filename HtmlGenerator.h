#ifndef HTML_GENERATOR_H
#define HTML_GENERATOR_H

#include <string>

class ContributionDataBase;

class HtmlGenerator {
public:
  HtmlGenerator(const ContributionDataBase &db);
  void Generate(const std::string &outputHtmlFilePath) const;

private:
  std::string GetChartJsContent() const;
  std::string ConvertEpochToString(const time_t &epoch) const;

  const ContributionDataBase &db;
};

#endif
