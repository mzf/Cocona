# Find ctemplate Library

find_path(CTEMPLATE_INCLUDE_PATH NAMES ctemplate/template.h)
find_library(CTEMPLATE_LIBRARY NAMES ctemplate)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ctemplate REQUIRED_VARS CTEMPLATE_LIBRARY CTEMPLATE_INCLUDE_PATH)


if (CTEMPLATE_FOUND)
  set(CTEMPLATE_INCLUDE_DIR  ${CTEMPLATE_INCLUDE_PATH})
  set(CTEMPLATE_INCLUDE_DIRS ${CTEMPLATE_INCLUDE_PATH})
  set(CTEMPLATE_LIBRARIES    ${CTEMPLATE_LIBRARY})
endif()

mark_as_advanced(
  CTEMPLATE_INCLUDE_PATH
  CTEMPLATE_LIBRARY
)
