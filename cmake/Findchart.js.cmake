find_path(CHARTJS_INCLUDE_PATH NAMES chart.min.js HINTS /usr/share/javascript/chart.js)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(chart.js REQUIRED_VARS CHARTJS_INCLUDE_PATH)

if (CHART.JS_FOUND)
  set(CHARTJS_INCLUDE_PATH  ${CHARTJS_INCLUDE_PATH})
endif()

mark_as_advanced(
  CHARTJS_INCLUDE_PATH
)

