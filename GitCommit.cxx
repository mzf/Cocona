#include "GitCommit.h"

#include "Git.h"
#include <git2.h>

GitCommit::GitCommit(git_repository &repo, const git_oid &objectId)
    : ObjectId(nullptr), Commit(nullptr) {
  this->ObjectId = new git_oid;
  auto errorcode = git_oid_cpy(this->ObjectId, &objectId);
  Git::CheckErrorCode(errorcode);

  errorcode = git_commit_lookup(&this->Commit, &repo, this->ObjectId);
  Git::CheckErrorCode(errorcode);
}

GitCommit::~GitCommit() {
  delete this->ObjectId;
  git_commit_free(this->Commit);
}

std::string GitCommit::GetAuthorEMail() const {
  const git_signature *author = git_commit_author(this->Commit);
  return author->email;
}

std::string GitCommit::GetId() const {
  char commitSumControl[GIT_OID_HEXSZ + 1] = {0};
  auto gitErrorCode = git_oid_fmt(commitSumControl, this->ObjectId);
  Git::CheckErrorCode(gitErrorCode);
  commitSumControl[GIT_OID_HEXSZ] = '\0';
  return commitSumControl;
}

time_t GitCommit::GetTime() const { return git_commit_time(this->Commit); }
