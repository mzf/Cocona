#include "ContributionDataBase.h"
#include "HtmlGenerator.h"
#include "UserConfig.h"

#include <iostream>

int main(int argc, char **argv) {
  std::cout << "+-------------------------------------------+" << std::endl;
  std::cout << "| COCONA, the COde CONtributions Aggregator |" << std::endl;
  std::cout << "+-------------------------------------------+" << std::endl;

  if (argc != 3) {
    std::cerr << "Need user config file and html output file as arguments."
              << std::endl;
    return EXIT_FAILURE;
  }

  std::cout << "Read User Config..." << std::endl;
  std::string userConfigFilePath(argv[1]);
  UserConfig userConfig(userConfigFilePath);

  std::cout << "Fill DataBase from repos..." << std::endl;
  ContributionDataBase db(userConfig);

  std::cout << "Generate HTML file..." << std::endl;
  HtmlGenerator generator(db);

  std::string outputHtml(argv[2]);
  generator.Generate(outputHtml);

  return EXIT_SUCCESS;
}
