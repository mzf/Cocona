#ifndef CONTRIBUTION_DATABASE_H
#define CONTRIBUTION_DATABASE_H

#include <memory>
#include <string>
#include <vector>
class UserConfig;
class SQLiteDataBase;

class ContributionDataBase {
public:
  ContributionDataBase(const UserConfig &userConfig);
  ~ContributionDataBase();

  const UserConfig &GetUserConfig() const;

  struct QueryCommitNumberResult {
    std::string repoName;
    std::vector<std::tuple<std::string, size_t>> commitNumberByTime;
  };

  int QueryTotalCommitNumber() const;
  time_t QueryFirstCommitEpoch() const;
  time_t QueryLastCommitEpoch() const;
  std::vector<QueryCommitNumberResult> QueryCommitNumberByRepoByYear() const;
  std::vector<QueryCommitNumberResult> QueryCommitNumberByRepoByMonth() const;
  std::vector<QueryCommitNumberResult> QueryCommitNumberByRepoByDay() const;

private:
  void CreateSQLiteDataBaseTables();
  void FillDataBase(const UserConfig &userConfig);
  std::vector<std::tuple<size_t, std::string>> QueryRepositories() const;
  std::vector<QueryCommitNumberResult>
  QueryCommitNumberByTimeSpan(const std::string &timeSpan) const;

  std::unique_ptr<SQLiteDataBase> db;
  const UserConfig &userConfig;
};

#endif // CONTRIBUTION_DATABASE_H
