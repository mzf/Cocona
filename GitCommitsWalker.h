#ifndef GITCOMMITSWALKER_H
#define GITCOMMITSWALKER_H

#include <memory>
class GitCommit;
class git_revwalk;
class git_repository;
class git_oid;

class GitCommitsWalker {
public:
  GitCommitsWalker(git_repository &repo);
  ~GitCommitsWalker();

  std::shared_ptr<GitCommit> GetNextCommit();

private:
  git_revwalk *Walker;
  git_repository &Repo;
};

#endif // GITCOMMITSWALKER_H
