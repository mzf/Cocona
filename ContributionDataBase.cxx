#include "ContributionDataBase.h"
#include "Git.h"
#include "GitCommit.h"
#include "GitCommitsWalker.h"
#include "GitRepository.h"
#include "SQLiteDataBase.h"
#include "TemporaryDirectory.h"
#include "UserConfig.h"

#include <filesystem>
#include <iostream>
#include <sstream>
#include <stdexcept>

#define DATABASE_PATH ":memory:"
// For Debugging:
//#define DATABASE_PATH "/tmp/db.sqlite"

ContributionDataBase::ContributionDataBase(const UserConfig &userConfig)
    : db(std::make_unique<SQLiteDataBase>(DATABASE_PATH)),
      userConfig(userConfig) {
  CreateSQLiteDataBaseTables();
  FillDataBase(userConfig);
}
ContributionDataBase::~ContributionDataBase() {}

void ContributionDataBase::CreateSQLiteDataBaseTables() {

  // Create repositories Table
  const std::string sql_repositories = "CREATE TABLE repositories("
                                       "repo_id INTEGER PRIMARY KEY,"
                                       "name    TEXT            NOT NULL,"
                                       "URI     TEXT            NOT NULL"
                                       ");";
  this->db->RunQuery(sql_repositories);

  // Create Entry Table
  const std::string sql_entries = "CREATE TABLE entries("
                                  "commit_id TEXT     NOT NULL,"
                                  "email     TEXT     NOT NULL,"
                                  "epoch     INT      NOT NULL,"
                                  "repo_id   INTEGER  NOT NULL,"
                                  "FOREIGN KEY (repo_id)"
                                  "REFERENCES repositories (repo_id)"
                                  ");";
  this->db->RunQuery(sql_entries);
}

void ContributionDataBase::FillDataBase(const UserConfig &userConfig) {
  TemporaryDirectory temporaryDirectory;
  Git git;

  unsigned int repoId = 0;
  for (auto repository : userConfig.GetRepositories()) {
    std::cout << " Processing: " << repository << "..." << std::endl;
    const char *url = repository.c_str();
    repoId++;

    // Add Repository to the DB.
    std::stringstream repoCreationQuery;
    repoCreationQuery << "INSERT INTO repositories (repo_id,name,URI) ";
    repoCreationQuery << "VALUES (" << repoId << ", 'repo" << repoId << "', '"
                      << url << "');";
    this->db->RunQuery(repoCreationQuery.str());

    // Create the git repository.
    std::stringstream tempRepoPathStream;
    tempRepoPathStream << temporaryDirectory.GetFullPath() << "/repo" << repoId;
    auto gitRepository = git.GetGitRepository(url, tempRepoPathStream.str());
    auto gitCommitsWalker = gitRepository->CreateCommitsWalker();

    // Walk the commits and create entries.
    while (auto commit = gitCommitsWalker->GetNextCommit()) {
      auto authorEmail = commit->GetAuthorEMail();
      bool isEntityFound = false;
      for (auto entity : userConfig.GetIdentities()) {
        if (authorEmail == entity) {
          isEntityFound = true;
        }
      }

      if (isEntityFound) {
        // Add Entry in DataBase.
        std::stringstream entryCreationQuery;
        entryCreationQuery
            << "INSERT INTO entries (commit_id,email,epoch,repo_id)";
        entryCreationQuery << "VALUES ('" << commit->GetId() << "', '"
                           << authorEmail << "', '" << commit->GetTime()
                           << "', " << repoId << ");";
        this->db->RunQuery(entryCreationQuery.str());
      }
    }
  }
}

const UserConfig &ContributionDataBase::GetUserConfig() const {
  return this->userConfig;
}

int ContributionDataBase::QueryTotalCommitNumber() const {
  std::string query("SELECT COUNT(*) FROM entries;");
  auto result = this->db->RunSelect(query);
  return std::get<int>(result[0].first);
}
time_t ContributionDataBase::QueryFirstCommitEpoch() const {
  std::string query("SELECT epoch FROM entries ORDER BY epoch ASC LIMIT 1;");
  auto result = this->db->RunSelect(query);
  return std::get<int>(result[0].first);
}
time_t ContributionDataBase::QueryLastCommitEpoch() const {
  std::string query("SELECT epoch FROM entries ORDER BY epoch DESC LIMIT 1;");
  auto result = this->db->RunSelect(query);
  return std::get<int>(result[0].first);
}

std::vector<ContributionDataBase::QueryCommitNumberResult>
ContributionDataBase::QueryCommitNumberByRepoByYear() const {
  return QueryCommitNumberByTimeSpan("%Y");
}

std::vector<ContributionDataBase::QueryCommitNumberResult>
ContributionDataBase::QueryCommitNumberByRepoByMonth() const {
  return QueryCommitNumberByTimeSpan("%Y-%m");
}

std::vector<ContributionDataBase::QueryCommitNumberResult>
ContributionDataBase::QueryCommitNumberByRepoByDay() const {
  return QueryCommitNumberByTimeSpan("%Y-%m-%d");
}

std::vector<ContributionDataBase::QueryCommitNumberResult>
ContributionDataBase::QueryCommitNumberByTimeSpan(
    const std::string &timeSpan) const {
  std::vector<ContributionDataBase::QueryCommitNumberResult> results;
  auto repositories = QueryRepositories();
  for (auto repo : repositories) {
    std::stringstream query;
    query << "SELECT strftime('" << timeSpan
          << "', date(epoch, 'unixepoch')) as timespan, "
             "COUNT(*) as count FROM entries WHERE repo_id="
          << std::get<0>(repo) << " GROUP BY timespan";
    auto queryResults = this->db->RunSelect(query.str());
    QueryCommitNumberResult result;
    result.repoName = std::get<1>(repo);
    for (auto &queryResult : queryResults) {
      result.commitNumberByTime.emplace_back(
          std::get<std::string>(queryResult.first),
          std::get<int>(queryResult.second));
    }
    results.emplace_back(std::move(result));
  }
  return results;
}

std::vector<std::tuple<size_t, std::string>>
ContributionDataBase::QueryRepositories() const {
  std::vector<std::tuple<size_t, std::string>> repositories;
  std::string query("SELECT repo_id, name FROM repositories;");
  auto results = this->db->RunSelect(query);
  for (auto &result : results) {
    repositories.emplace_back(std::get<int>(result.first),
                              std::get<std::string>(result.second));
  }
  return repositories;
}
