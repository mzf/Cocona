#include "TemporaryDirectory.h"

TemporaryDirectory::TemporaryDirectory() {
  this->Path = {std::filesystem::temp_directory_path() /= std::tmpnam(nullptr)};
  std::filesystem::create_directories(this->Path);
}

TemporaryDirectory::~TemporaryDirectory() {
  std::filesystem::remove_all(this->Path);
}

std::string TemporaryDirectory::GetFullPath() const { return this->Path; }
