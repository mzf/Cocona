import pygit2
import tempfile
import subprocess
import sys
import difflib

def create_git_repo(repo_dir):
    return pygit2.init_repository(repo_dir, False)

def commit_file(is_first_commit, repo, repo_dir, filename, author, committer, message):
    with open(repo_dir + '/' + filename, 'w') as f:
        f.write("Dummy Content")
    repo.index.add_all()
    repo.index.write()
    tree = repo.index.write_tree()
    if(is_first_commit):
        ref = "HEAD"
        parents = []
    else:
        ref = repo.head.name
        parents = [repo.head.target]
    repo.create_commit(ref, author, committer, message, tree, parents)

with tempfile.TemporaryDirectory() as temporary_dir:
    print(f'temporary directory for git repos: {temporary_dir}')

    # create repositories
    alice_signature = pygit2.Signature('Alice Author', 'alice@authors.tld', 2399662595, 0)
    bob_signature = pygit2.Signature('Bob Author', 'bob@authors.tld', 2399662596, 0)
    cecile_signature = pygit2.Signature('Cecil Committer', 'cecil@committers.tld', 2399662597, 0)

    repo1_dir = temporary_dir + '/repo1';
    repo1 = create_git_repo(repo1_dir)
    commit_file(True, repo1, repo1_dir, 'test1.txt', alice_signature, cecile_signature, "Repo1 First Alice Commit.")
    commit_file(False, repo1, repo1_dir, 'test2.txt', alice_signature, cecile_signature, "Repo1 Second Alice Commit.")
    commit_file(False, repo1, repo1_dir, 'test3.txt', bob_signature, cecile_signature, "Repo1 Bob Stuff.")

    repo2_dir = temporary_dir + '/repo2';
    repo2 = create_git_repo(repo2_dir)
    commit_file(True, repo2, repo2_dir, 'test4.txt', alice_signature, cecile_signature, "Repo2 First Alice Commit.")
    commit_file(False, repo2, repo2_dir, 'test5.txt', alice_signature, cecile_signature, "Repo2 Second Alice Commit.")
    commit_file(False, repo2, repo2_dir, 'test6.txt', bob_signature, cecile_signature, "Repo2 Bob Stuff.")

    # create config file
    config_file_path = temporary_dir + '/config.yaml'
    with open(config_file_path, 'w') as config_file:
        config_file.write('repositories:\n')
        config_file.write(f' - {repo1_dir}\n')
        config_file.write(f' - {repo2_dir}\n')
        config_file.write('identities:\n')
        config_file.write(f' - {alice_signature.email}\n')
        config_file.write('profile:\n')
        config_file.write('  picture: "profile.png"\n')
        config_file.write('  name: "Alice Contributor"\n')

    # launch cocona
    #output_html_path = temporary_dir + '/output.html'
    output_html_path = '/tmp/output.html'
    cocona_exec = sys.argv[1]
    process_status = subprocess.run([cocona_exec, config_file_path, output_html_path])
    process_status.check_returncode()

    # check output html
    reference_output_file_path = sys.argv[2]
    with open(reference_output_file_path, 'r') as reference_file:
        reference_lines = reference_file.readlines()
        with open(output_html_path, 'r') as output_file:
            output_lines = output_file.readlines()
            output_lines_filtered = [n.replace(temporary_dir, '') for n in output_lines]
            diff_result = difflib.unified_diff(output_lines_filtered, reference_lines, fromfile=reference_output_file_path, tofile=output_html_path)
            string_diff_result = ''.join(diff_result)
            if string_diff_result:
                print(f"FAILURE, diff detected:\n{string_diff_result}")
                exit(1)








