#ifndef GIT_REPOSITORY_H
#define GIT_REPOSITORY_H

#include <memory>
#include <string>

class GitCommitsWalker;
class git_repository;

class GitRepository {
public:
  GitRepository(const std::string &url, const std::string &localPath);
  ~GitRepository();

  std::shared_ptr<GitCommitsWalker> CreateCommitsWalker();

private:
  git_repository *Repo = nullptr;
};

#endif // GIT_REPOSITORY_H
